Rails.application.routes.draw do

  root 'static_pages#home'

  get '/home',    to: 'static_pages#home'
  get '/help',    to: 'static_pages#help'
  get '/contact', to: 'static_pages#contact'

  # get '/signup',  to: 'users#new'
  # post '/signup',  to: 'users#create'

  # post '/login', to: 'users/sessions#create'
  # get '/login', to: 'users/sessions#new'
  # delete '/logout', to: 'sessions#destroy'

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    sessions: 'users/sessions'
  },
  path_names: { sign_in: 'login', sign_out: 'logout'}

  authenticated :user do
    root "users#show"
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users, only: [:show, :create, :new] do
    root "users#show"
  end

  resources :polls do
    root "polls#index"
  end
end
