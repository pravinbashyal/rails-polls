class Vote extends React.Component {

  constructor (props) {
    super (props)
    this.state = props.poll
  }

  handleSelect (ansId) {
    const pollId = this.state.id
    const answers = Object.assign([], this.state.answers)

    this.checkVote(pollId) && this.deductVote(answers, localStorage.getItem('voteData' + pollId))

    this.addVote(answers, ansId)

    localStorage.setItem('voteData'+ pollId, ansId)


    this.setState({
      answers: answers
    })

    this.updateVote(this.state)
  }

  updateVote (pollData) {
    const dataFields = ['question', 'answers', 'user_id']
    const ajaxData = {};
    dataFields.forEach ( (dataField) => {
      ajaxData[dataField] = pollData[dataField]
    })
    $.ajax({
      url: "/polls/" + pollData.id,
      type: "PATCH",
      data: ajaxData
    })
  }

  checkVote (pollId) {
    let voteData = localStorage.getItem('voteData'+pollId)
    return voteData !== null //|| Number(voteData) === Number(ansId)
  }

  checked (id, pollId) {
    const voted = localStorage.getItem('voteData'+pollId)
    return voted !== null && Number( id )=== Number (localStorage.getItem('voteData'+pollId))
  }

  deductVote (answers, ansId) {
    const answer = answers[ansId]
    answer.count = Number(answer.count) - 1
    answers[ansId] = answer
    return answers
  }

  addVote (answers, ansId) {
    const answer = answers[ansId]
    answer.count = Number(answer.count) + 1
    answers[ansId] = answer
    return answers
  }

  render () {
    return (
      <div className="poll">
        <div className="answers-block">
          {this.props.poll.answers.map((ans, id) => {
          return React.createElement(Option, {
          key: id,
          answer: ans,
          checked: this.checked(id, this.props.poll.id),
          id: id,
          handleSelect: this.handleSelect.bind(this)})
            })}
        </div>

        <div className="answers-block col-md-12 chart-block">
          {React.createElement(Chart, this.state)}
        </div>

      </div>
    )
  }
}
