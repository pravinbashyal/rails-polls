class Option extends React.Component {

  constructor () {
    super ()
    this.handleSelect = this.handleSelect.bind(this)
  }

  handleSelect (e) {
    this.props.handleSelect(e.target.id)
  }

  render () {
    return (
      <div className="col-md-12 answer border" >
        <input
          type="radio"
          className="col-md-1"
          onChange={this.handleSelect}
          name="poll"
          checked={this.props.checked}
          id={this.props.id}/>
        <label
          className="col-md-11 answer-label"
          htmlFor={this.props.id}>
          {this.props.answer.text}
          <span className="pull-right">{this.props.answer.count}</span>
        </label>
      </div>
    )
  }
}
