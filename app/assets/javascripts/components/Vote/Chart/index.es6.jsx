
class Chart extends React.Component {

  structureData (data) {
    const datum = {
      key: "Results",
      values: data
    }
    return datum
  }

  render () {

    const chart = {
        id:'barChart',
        type:'discreteBarChart',
        datum: [this.structureData(this.props.answers)],
        x: 'text',
        y: 'count'
    }

    return (
      <div className="bar-chart">
        {React.createElement(NVD3Chart, chart)}
      </div>
    )
  }
}
