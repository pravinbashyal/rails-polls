class AnswerBox extends React.Component {

  constructor () {
    super ()
  }

  handleAnswerChange (e) {
    this.props.handleAnswerChange(e)
  }

  render () {
    return (
      <div className="answer col-md-12">
        <input
          type="text"
          id={this.props.id}
          className="form-control"
          onBlur = {this.handleAnswerChange.bind(this)}
        />
      </div>
    )
  }
}
