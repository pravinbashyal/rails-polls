class FormCreate extends React.Component {

  constructor () {
    super ()

    this.state = {
      question: "",
      answers: [{
        text: "",
        count: 0
      }]
    }
  }

  handleAddMore (answer = { text: "", count: 0 }) {
    this.setState({
      answers: [ ...this.state.answers, answer ]
    })
  }

  handleQuestionChange ( e ) {
    this.setState ({
      question : e.target.value
    })
  }

  handleAnswerChange ( element ) {
    let id = element.target.id
    let value = element.target.value
    let answers = Object.assign([], this.state.answers)
    answers[id] = {
      text: value,
      count:0
    }
    this.setState({
      answers: answers
    })
  }

  handleSubmit () {
    $.post('/polls',
      this.state,
      (res, err) => {
        res ? console.log(res): console.log(err)
      }
    )
  }

  generateRandom () {
    return Math.random().toString(36).substr(2, 7);
  }


  render () {
    return (
      <div>
        <label for="question" className="same-line">Question</label>
        <input
          type="text"
          className="form-control"
          id="question"
          onBlur={this.handleQuestionChange.bind(this)}/>
        <label>Answers</label>
        { this.state.answers.map ( (answer, index) => {
          return React.createElement(AnswerBox, {
            key: index,
            id: index,
            handleAnswerChange: this.handleAnswerChange.bind(this)
          })
        }) }
        { React.createElement(AddMore, { addMore: this.handleAddMore.bind(this) }) }
        <div className="row vertical-space">
          <button className="btn btn-primary" onClick={this.handleSubmit.bind(this)}>Create</button>
        </div>
      </div>
    )
  }
}
