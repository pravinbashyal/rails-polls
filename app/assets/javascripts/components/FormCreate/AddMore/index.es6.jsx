class AddMore extends React.Component {

  constructor () {
    super ()
  }

  handleClick () {
    this.props.addMore({
      text: "",
      count: 0
    })
  }

  render () {
    return (
      <div className="dotted add-more">
        <span className="grey col-md-12 center" onClick={this.handleClick.bind(this)}>Add More</span>
      </div>
    )
  }
}
