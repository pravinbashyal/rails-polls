class Poll < ApplicationRecord
  belongs_to :user

  # has_many :answers

  validates :question, presence: true
  validates :answers, presence: true

  # accepts_nested_attributes_for :answers
end
