class PollsController < ApplicationController

  before_action :authenticate_user!, only: [:new, :create]

  def index
    @polls = Poll.all
  end

  def new
  end

  def show
    @poll = Poll.find(params[:id])
  end

  def create
    build_user
    poll = Poll.new(poll_params)
    if poll.save
      flash[:success] = "Poll has been created"
      redirect_to :back
    else
      flash[:warning] = "could not create because " + poll.errors.full_messages.join(', ')
      redirect_to :back
    end
  end

  def update
    poll = Poll.find(params[:id])
    if poll.update(poll_params)
      flash[:success] = "VOTED"
    else
      flash[:warning] = "could not create because " + poll.errors.full_messages.join(', ')
      redirect_to :back
    end
  end

  def poll_params
    params[:answers] = params[:answers].map{|k, v| v}
    params.permit(:question, :user_id, :answers => [:text, :count])
  end

  def build_user
    params[:user_id] = current_user.id
  end

end
