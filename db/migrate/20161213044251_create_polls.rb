class CreatePolls < ActiveRecord::Migration[5.0]
  def change
    create_table :polls do |t|
      t.string :question
      t.json :answers

      t.references :user, index: true

      t.timestamps
    end
  end
end
